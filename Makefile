# itex2MML = ~/sandbox/itex2mml/itex/itex2MML
# TIDY = ~/sandbox/itex2mml/tidyExp/tidy -4mz -q

allhtml: $(shell find . -name "*.rst.txt" | sed 's/rst\.txt/rst\.html/')
# allxhtml: $(shell find . -name "*.rst.txt" | sed 's/rst\.txt/rst\.xhtml/')
allpdf: $(shell find . -name "*.rst.txt" | sed 's/rst\.txt/rst\.pdf/')
alltex: $(shell find . -name "*.rst.txt" | sed 's/rst\.txt/rst\.tex/')
# all: allxhtml allpdf
all: allhtml allpdf

%.rst.html: %.rst.txt
	rst2html --stylesheet=$(PWD)/recursos/estilo.css --link-stylesheet $< > $@

%.rst.tex: %.rst.txt
	rst2latex --hyperlink-color 0 $< > $@

%.rst.pdf: %.tex
	pdflatex $<
	pdflatex $<

# %.xhtml: %.html
#         sed "s/\\$$\\$$\(.*\)\\$$\\$$/\\\[\1\\\]/g" $< | $(TIDY)  | \
#         $(itex2MML) > $@

# %.png: %.plot
# 	gnuplot $< > $@

clean:
	find . -type f \( -name '*.rst.html' -o -name '*.rst.pdf' -o -name '*.aux' -o -name '*.rst.tex' -o -name '*.out' -o -name '*.log' -o -name '*.rst.xhtml' \) -exec rm -f '{}' \;
